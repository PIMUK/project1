﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    public GameObject Enemyship;

    private float maxSpawner = 1f;
    
    // Start is called before the first frame update
    void Start()
    {
        Invoke("SpawnerEnemy", maxSpawner);
        
        InvokeRepeating("IncreaseSpawnerRate", 0f, 30f);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void SpawnerEnemy()
    {
        Vector2 min = Camera.main.ViewportToWorldPoint(new Vector2(0, 0));

        Vector2 max = Camera.main.ViewportToWorldPoint(new Vector2(1, 1));

        GameObject anEnemy = (GameObject) Instantiate(Enemyship);
        anEnemy.transform.position = new Vector2(Random.Range(min.x, max.x), max.y);
    }

    void ScheduleNextEnemyspawner()
    {
        float spawnerInSeconds;
        if (maxSpawner > 1f)
        {
            spawnerInSeconds = Random.Range(1f, maxSpawner);
        }
        else
        {
            spawnerInSeconds = 1f;
            Invoke("SpawnerEnemy", spawnerInSeconds);
        }

        void IncreaseSpawnerRate()
        {
            if (maxSpawner > 1F)
                maxSpawner--;

            if (maxSpawner == 1f)
                CancelInvoke("IncreaseSpawnerRate");
        }
    }
}
